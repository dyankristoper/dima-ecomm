<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'server_pharm_asia' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-:b:;FEAr#H+^<}U{AKLF|hHhTvov|mL>X,qfR4a2;K^wytdYG[+xo:k:b5QaX+_' );
define( 'SECURE_AUTH_KEY',  'kHzybN>FJT2u9gxW8@G0hvY%ggN@C[p9Bx7)nl)P!f<td#9+}dG98.JI2/p&:eOF' );
define( 'LOGGED_IN_KEY',    'fXavBnN1-U~tu,:Ri$2g%v6L/1;oKe|mj j-&}R_2Oa{TeK7od]x{VlI^Z=@?,vT' );
define( 'NONCE_KEY',        '*K./[I1+iI-*$$TuA7&}A37(+s6Jb`j?()BGl1ZjzuGC=.|C4|<HD3FB R g-g-I' );
define( 'AUTH_SALT',        'tJKUB$S6Tw.S,|`:RDTq?},@(_alZ4d6qgqLifgiA{15N-){)CPdzlm[>p`,L<y,' );
define( 'SECURE_AUTH_SALT', 'I54e(~n,Kbx3H-Nh|RHmKis*(aJujW/Ajq^NP76T_Ren6{6ZN.uZn)^P_Sg 5B`C' );
define( 'LOGGED_IN_SALT',   'Lp;~b7nN;aSDt3Jl|[3FC@A<7b!T/7*2A9|fFd@<oL; .JMpSfd=bf3k0}jW-_Qz' );
define( 'NONCE_SALT',       '*wb@X~6LM>:n(C>Ur4}F%`&(IL|tha=?`f;I>f(*Dsl.}GcYEV>,=IBWY4l>~6]u' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'phasia_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
